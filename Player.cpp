#include "include/Player.h"
#include "include/World.h"

void Player::update(float delta){
    if(delta > 1) return;
    move(delta);
    velocityY += gravity*delta;
    if(world.isBlockAt(position.x, position.y - 1, position.z) && velocityY < 0){
        velocityY = 0;
    }
    position.y += velocityY * delta;
}

void Player::move(float delta){

}
