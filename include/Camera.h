#ifndef CAMERA_H
#define CAMERA_H

#include <math.h>
#include "Position3.h"
#include "Utils.h"

class Camera
{
    Position3 position;
    Position3 lookAtPosition;

    float horizontalRotation = 0;
    float verticalRotation = 0;

    public:
        Camera() = default;

        void lookAt() const{
            gluLookAt(position.x, position.y, position.z,
                      lookAtPosition.x, lookAtPosition.y, lookAtPosition.z,
                      0, 1, 0
                      );
        }

        void setPosition(Position3 pos){
            position = pos;
        }

        void setLookAtPosition(Position3 pos){
            lookAtPosition = pos;
        }

        void setPosition(float x, float y, float z){
            position.x = x;
            position.y = y;
            position.z = z;
        }

        void setLookAtPosition(float x, float y, float z){
            lookAtPosition.x = x;
            lookAtPosition.y = y;
            lookAtPosition.z = z;
        }

        void setRotation(float horizontal, float vertical){
            horizontalRotation = horizontal;
            verticalRotation = vertical;

            while(horizontalRotation > 360) horizontalRotation-=360;
            while(verticalRotation > 360) verticalRotation-=360;

        }

        void rotateBy(float horizontal, float vertical){
            horizontalRotation += horizontal;
            verticalRotation += vertical;
            if(verticalRotation > 160) verticalRotation = 160;
            if(verticalRotation < 20) verticalRotation = 20;

            setRotation(horizontalRotation, verticalRotation);
        }

        void rotationToLookAtPosition(){
            lookAtPosition.x = position.x + cos(Utils::degreesToRad(horizontalRotation))*sin(Utils::degreesToRad(verticalRotation));
            lookAtPosition.y = position.y + cos(Utils::degreesToRad(verticalRotation));
            lookAtPosition.z = position.z + sin(Utils::degreesToRad(horizontalRotation))*sin(Utils::degreesToRad(verticalRotation));
        }

        Position3& getPosition(){
            return position;
        }

        Position3& getLookAtPosition(){
            return lookAtPosition;
        }

};

#endif // CAMERA_H
