#ifndef CUBE_H
#define CUBE_H

#include "Texture.h"

class Cube
{
    GLuint up;
    GLuint side;
    public:

        Cube() = default;

        Cube(GLuint up, GLuint side) {
            this->up = up;
            this->side = side;
        }

        void setTextures(GLuint up, GLuint side) {
            this->up = up;
            this->side = side;
        }


    void voxelRender(float x, float y, float z, float lightTop, float lightBottom,
                     float lightE, float lightW, float lightN, float lightS) const {


        glBindTexture(GL_TEXTURE_2D, side);
        glBegin(GL_QUADS);
            glColor3f(lightS, lightS, lightS);
            glTexCoord2i(0,0);
            glVertex3f(x,y,z);
            glTexCoord2i(1,0);
            glVertex3f(x +1.0,y,z);
            glTexCoord2i(1,1);
            glVertex3f(x +1.0, y +1.0, z);
            glTexCoord2i(0,1);
            glVertex3f(x, y +1.0, z);

            glColor3f(lightN, lightN, lightN);
            glTexCoord2i(0,0);
            glVertex3f(x,y,z+1.0);
            glTexCoord2i(1,0);
            glVertex3f(x+1.0,y,z+1.0);
            glTexCoord2i(1,1);
            glVertex3f(x+1.0,y+1.0,z+1.0);
            glTexCoord2i(0,1);
            glVertex3f(x,y+1.0,z+1.0);

            glColor3f(lightW, lightW, lightW);
            glTexCoord2i(0,0);
            glVertex3f(x,y,z);
            glTexCoord2i(1,0);
            glVertex3f(x,y,z+1.0);
            glTexCoord2i(1,1);
            glVertex3f(x,y+1.0,z+1.0);
            glTexCoord2i(0,1);
            glVertex3f(x,y+1.0,z);

            glColor3f(lightE, lightE, lightE);
            glTexCoord2i(0,0);
            glVertex3f(x+1.0,y,z);
            glTexCoord2i(1,0);
            glVertex3f(x+1.0,y,z+1.0);
            glTexCoord2i(1,1);
            glVertex3f(x+1.0,y+1.0,z+1.0);
            glTexCoord2i(0,1);
            glVertex3f(x+1.0,y+1.0,z);
        glEnd();

        glBindTexture(GL_TEXTURE_2D, up);
        glBegin(GL_QUADS);
            glColor3f(lightBottom, lightBottom, lightBottom);
            glTexCoord2i(0,0);
            glVertex3f(x,y,z);
            glTexCoord2i(1,0);
            glVertex3f(x,y,z+1.0);
            glTexCoord2i(1,1);
            glVertex3f(x+1.0,y,z+1.0);
            glTexCoord2i(0,1);
            glVertex3f(x+1.0,y,z);

            glColor3f(lightTop, lightTop, lightTop);
            glTexCoord2i(0,0);
            glVertex3f(x,y+1.0,z);
            glTexCoord2i(1,0);
            glVertex3f(x,y+1.0,z+1.0);
            glTexCoord2i(1,1);
            glVertex3f(x+1.0,y+1.0,z+1.0);
            glTexCoord2i(0,1);
            glVertex3f(x+1.0,y+1.0,z);

        glEnd();
    }

};

#endif // CUBE_H
