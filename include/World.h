#ifndef WORLD_H
#define WORLD_H

#include <iostream>
#include <stdlib.h>
#include <deque>

enum class BlockType{
    AirBlock,
    GrassBlock,
    StoneBlock,
    WoodBlock,
    LeafBlock,
    CobblestoneBlock,
    DiamondBlock,
    IronBlock,
    SandBlock
};

#include "Blocks/Block.h"
#include "Camera.h"
#include "CubeGenerator.h"
#include "Player.h"
#include "Blocks/AirBlock.h"
#include "Blocks/GrassBlock.h"
#include "Blocks/StoneBlock.h"
#include "Blocks/WoodBlock.h"
#include "Blocks/LeafBlock.h"
#include "Blocks/CobblestoneBlock.h"
#include "Blocks/DiamondBlock.h"
#include "Blocks/IronBlock.h"
#include "Blocks/SandBlock.h"

struct Chunk{
    Block* blocks[16][16][16];

    void render(int x, int y, int z){
        for(int xx = 0; xx < 16; xx++){
            for(int yy = 0; yy < 16; yy++){
                for(int zz = 0; zz < 16; zz++){
                    blocks[xx][yy][zz]->render(xx + x*16, yy + y*16, zz + z*16);
                }
            }
        }
    }
};

class World
{
    public:
    static const int CHUNKS_X = 4;
    static const int CHUNKS_Y = 4;
    static const int CHUNKS_Z = 4;
    static const int WORLD_SIZE_X = CHUNKS_X*16;
    static const int WORLD_SIZE_Y = CHUNKS_Y*16;
    static const int WORLD_SIZE_Z = CHUNKS_Z*16;
    private:
    Chunk chunks[CHUNKS_X][CHUNKS_Y][CHUNKS_Z];

    private:

    Camera camera;
    Player player;

    public:
        World() : player(*this){
            for(int x = 0; x < WORLD_SIZE_X; x++){
                for(int y = 0; y < WORLD_SIZE_Y; y++){
                    for(int z = 0; z < WORLD_SIZE_Z; z++){
                        setBlockAt(x, y, z, BlockType::AirBlock);
                    }
                }
            }
        }

        void mouseMoved(int fromX, int fromY, int toX, int toY){

        }

        void mousePressed(int button){

        }

        void keyPressed(unsigned char key){
            if(key == 27) exit(0);
        }

        void keyReleased(unsigned char key){

        }

        void update(float delta){
            player.update(delta);
            camera.setPosition(player.getEyesPosition());
            camera.setRotation(player.getRotation(), player.getHeadAngle());
            camera.rotationToLookAtPosition();
        }

        void addTree(int x, int y, int z);

        void render(){
            for(int x = 0; x < CHUNKS_X; x++){
                for(int y = 0; y < CHUNKS_Y; y++){
                    for(int z = 0; z < CHUNKS_Z; z++){
                        float dx = abs(x*16 - player.getPosition().x);
                        float dz = abs(z*16 - player.getPosition().z);
                        if(dx < 50 && dz < 50){
                            chunks[x][y][z].render(x, y, z);
                        }

                    }
                }
            }
        }

        void calculateVisibility(){
            for(int x = 1; x < WORLD_SIZE_X - 1; x++){
                for(int y = 1; y < WORLD_SIZE_Y - 1; y++){
                    for(int z = 1; z < WORLD_SIZE_Z - 1; z++){
                        if( getBlockAt(x, y + 1, z)->getType() == BlockType::AirBlock ||
                            getBlockAt(x, y - 1, z)->getType() == BlockType::AirBlock ||
                            getBlockAt(x + 1, y, z)->getType() == BlockType::AirBlock ||
                            getBlockAt(x - 1, y, z)->getType() == BlockType::AirBlock ||
                            getBlockAt(x, y, z + 1)->getType() == BlockType::AirBlock ||
                            getBlockAt(x, y, z - 1)->getType() == BlockType::AirBlock)
                            {
                                getBlockAt(x, y, z)->setVisible(true);
                            }
                            else{
                                getBlockAt(x, y, z)->setVisible(false);
                            }
                    }
                }
            }
        }

        void setVisibleAround(int x, int y, int z){
            if(y < WORLD_SIZE_Y - 1) getBlockAt(x, y + 1, z)->setVisible(true);
            if(y > 0) getBlockAt(x, y - 1, z)->setVisible(true);
            if(x < WORLD_SIZE_X - 1) getBlockAt(x + 1, y, z)->setVisible(true);
            if(x > 0) getBlockAt(x - 1, y, z)->setVisible(true);
            if(z < WORLD_SIZE_Z - 1) getBlockAt(x, y, z + 1)->setVisible(true);
            if(z > 0) getBlockAt(x, y, z - 1)->setVisible(true);
        }

        void addAllAirBlocksAround(int x, int y, int z, std::deque<Position3>& q){
            if(y < WORLD_SIZE_Y - 1 && getBlockAt(x, y + 1, z)->getType() == BlockType::AirBlock){
                q.push_back(Position3(x,y+1,z));
            }
            if(y > 0 && getBlockAt(x, y - 1, z)->getType() == BlockType::AirBlock){
                q.push_back(Position3(x,y-1,z));
            }
            if(x < WORLD_SIZE_X - 1 && getBlockAt(x +1, y, z)->getType() == BlockType::AirBlock){
                q.push_back(Position3(x+1,y,z));
            }
            if(x > 0 && getBlockAt(x - 1, y, z)->getType() == BlockType::AirBlock){
                q.push_back(Position3(x - 1,y,z));
            }
            if(z < WORLD_SIZE_Z - 1 && getBlockAt(x, y, z + 1 )->getType() == BlockType::AirBlock){
                q.push_back(Position3(x,y,z +1));
            }
            if(z > 0 && getBlockAt(x, y, z - 1)->getType() == BlockType::AirBlock){
                q.push_back(Position3(x,y,z - 1));
            }
        }

        void create();

        void setBlockAt(int x, int y, int z, BlockType type){

            if(x < 0 || y < 0 || z < 0 ||
               x >= WORLD_SIZE_X ||
               y >= WORLD_SIZE_Y ||
               z >= WORLD_SIZE_Z) return;

            if(getBlockAt(x,y,z) != nullptr) delete getBlockAt(x, y, z);

            switch(type){
                case BlockType::AirBlock: setBlockTo(x,y,z, new AirBlock()); break;
                case BlockType::GrassBlock: setBlockTo(x,y,z, new GrassBlock()); break;
                case BlockType::StoneBlock: setBlockTo(x,y,z, new StoneBlock()); break;
                case BlockType::WoodBlock: setBlockTo(x,y,z, new WoodBlock()); break;
                case BlockType::LeafBlock: setBlockTo(x,y,z, new LeafBlock()); break;
                case BlockType::CobblestoneBlock: setBlockTo(x,y,z, new CobblestoneBlock()); break;
                case BlockType::DiamondBlock: setBlockTo(x,y,z, new DiamondBlock()); break;
                case BlockType::IronBlock: setBlockTo(x,y,z, new IronBlock()); break;
                case BlockType::SandBlock: setBlockTo(x,y,z, new SandBlock()); break;
            }

        }

        bool isBlockAt(float x, float y, float z){
            if(x < 0 || y < 0 || z < 0 || x >= WORLD_SIZE_X || y >= WORLD_SIZE_Y || z >= WORLD_SIZE_Z){
                return false;
            }
            return getBlockAt(static_cast<int>(x),static_cast<int>(y),static_cast<int>(z))->collides();
        }

        const Camera& getCamera(){
            return camera;
        }

        ~World(){
            for(int x = 0; x < WORLD_SIZE_X; x++){
                for(int y = 0; y < WORLD_SIZE_Y; y++){
                    for(int z = 0; z < WORLD_SIZE_Z; z++){
                        delete getBlockAt(x, y, z);
                    }
                }
            }
        }

        void calculateLightValues(std::deque<Position3> q){
            while(!q.empty()){
                Position3 pos = q.front();
                q.pop_front();
                float lightValue = static_cast<AirBlock*>(getBlockAt(pos.x,pos.y,pos.z))->getLightValue();
                Block* b = getBlockAt(pos.x, pos.y - 1, pos.z);
                if(b && b->getType() == BlockType::AirBlock){
                    if(lightValue > 0.95){
                        static_cast<AirBlock*>(b)->setLightValue(lightValue);
                    }
                    else{
                        static_cast<AirBlock*>(b)->setLightValue(lightValue - 0.1);
                    }
                    q.push_back(Position3(pos.x, pos.y - 1, pos.z));
                }
                Block* e = getBlockAt(pos.x + 1, pos.y, pos.z);
                if(e && e->getType() == BlockType::AirBlock){
                    if(static_cast<AirBlock*>(e)->getLightValue() < lightValue){
                        static_cast<AirBlock*>(e)->setLightValue(lightValue - 0.1);
                        q.push_back(Position3(pos.x + 1, pos.y, pos.z));
                    }
                }
                Block* w = getBlockAt(pos.x - 1, pos.y, pos.z);
                if(w && w->getType() == BlockType::AirBlock){
                    if(static_cast<AirBlock*>(w)->getLightValue() < lightValue){
                        static_cast<AirBlock*>(w)->setLightValue(lightValue - 0.1);
                        q.push_back(Position3(pos.x - 1, pos.y, pos.z));
                    }
                }
                Block* n = getBlockAt(pos.x, pos.y, pos.z + 1);
                if(n && n->getType() == BlockType::AirBlock){
                    if(static_cast<AirBlock*>(n)->getLightValue() < lightValue){
                        static_cast<AirBlock*>(n)->setLightValue(lightValue - 0.1);
                        q.push_back(Position3(pos.x, pos.y, pos.z + 1));
                    }
                }
                Block* s = getBlockAt(pos.x, pos.y, pos.z - 1);
                if(s && s->getType() == BlockType::AirBlock){
                    if(static_cast<AirBlock*>(s)->getLightValue() < lightValue){
                        static_cast<AirBlock*>(s)->setLightValue(lightValue - 0.1);
                        q.push_back(Position3(pos.x, pos.y, pos.z - 1));
                    }
                }

            }
        }

        void calculateLight() {
            for(int x = 0; x < WORLD_SIZE_X; x++){
                for(int y = 0; y < WORLD_SIZE_Y; y++){
                    for(int z = 0; z < WORLD_SIZE_Z; z++){
                        Block* b = getBlockAt(x,y,z);
                        if(b->getVisible()){
                            Block* top = getBlockAt(x, y + 1, z);
                            Block* bot = getBlockAt(x, y - 1, z);
                            Block* e = getBlockAt(x + 1, y, z);
                            Block* w = getBlockAt(x - 1, y, z);
                            Block* s = getBlockAt(x, y, z - 1);
                            Block* n = getBlockAt(x, y, z + 1);
                            float tt = 0;
                            float bb = 0;
                            float ee = 0;
                            float ww = 0;
                            float ss = 0;
                            float nn = 0;
                            if(top && top->getType() == BlockType::AirBlock){
                                tt = static_cast<AirBlock*>(top)->getLightValue();
                            }
                            if(bot && bot->getType() == BlockType::AirBlock){
                                bb = static_cast<AirBlock*>(bot)->getLightValue();
                            }
                            if(e && e->getType() == BlockType::AirBlock){
                                ee = static_cast<AirBlock*>(e)->getLightValue();
                            }
                            if(w && w->getType() == BlockType::AirBlock){
                                ww = static_cast<AirBlock*>(w)->getLightValue();
                            }
                            if(s && s->getType() == BlockType::AirBlock){
                                ss = static_cast<AirBlock*>(s)->getLightValue();
                            }
                            if(n && n->getType() == BlockType::AirBlock){
                                nn = static_cast<AirBlock*>(n)->getLightValue();
                            }
                            b->setLightValues(tt, bb, ee - 0.2, ww - 0.2, nn - 0.2, ss-0.2);
                        }
                    }
                }
            }
        }

    private:
        void setBlockTo(int x, int y, int z, Block* b){
            chunks[x/16][y/16][z/16].blocks[x%16][y%16][z%16] = b;
        }

        Block* getBlockAt(int x, int y, int z){
            if(x < 0 || y < 0 || z < 0 || x >= WORLD_SIZE_X || y >= WORLD_SIZE_Y || z >= WORLD_SIZE_Z){
                return nullptr;
            }
            return chunks[x/16][y/16][z/16].blocks[x%16][y%16][z%16];
        }
};



#endif // WORLD_H
