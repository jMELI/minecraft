#ifndef ASSETS_H
#define ASSETS_H

#include "Texture.h"
#include <string>

class Assets
{
    public:
        static Texture grassTop;
        static Texture grassSide;
        static Texture stone;
        static Texture woodSide;
        static Texture woodUp;
        static Texture leaves;
        static Texture cobblestone;
        static Texture diamond;
        static Texture iron;
        static Texture sand;

        static void load(){
            grassTop.load();
            grassSide.load();
            stone.load();
            woodSide.load();
            woodUp.load();
            leaves.load();
            cobblestone.load();
            diamond.load();
            iron.load();
            sand.load();
        }

};

#endif // ASSETS_H
