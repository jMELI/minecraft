#ifndef CUBETEXTURE_H
#define CUBETEXTURE_H

#include "SOIL.h"
#include <cstring>

class CubeTexture
{
    GLuint texture;
    std::string Up;
    std::string Down;
    std::string North;
    std::string East;
    std::string South;
    std::string West;

    public:
        CubeTexture(std::string up, std::string side) {
            Up = up;
            Down = up;
            North = side;
            East = side;
            West = side;
            South = side;
        }

        void load(){
            texture = SOIL_load_OGL_cubemap
            (
                Up.c_str(),
                Up.c_str(),
                Up.c_str(),
                Up.c_str(),
                Up.c_str(),
                Up.c_str(),
                SOIL_LOAD_RGB,
                SOIL_CREATE_NEW_ID,
                SOIL_FLAG_MIPMAPS
            );
            if( 0 == texture )
            {
                std::cout << "SOIL loading error:" <<  SOIL_last_result() << std::endl;
            }
            else{
                std::cout << "Texture loaded :" <<  Up << std::endl;
            }
        }

        GLuint getTexture(){
            return texture;
        }
};

#endif // CUBETEXTURE_H
