#ifndef POSITION3_H
#define POSITION3_H


struct Position3
{
    float x = 0;
    float y = 0;
    float z = 0;

    Position3() = default;

    Position3(float xx, float yy, float zz){
        x=xx;
        y=yy;
        z=zz;
    }

    Position3& operator+=(const Position3& pos){
        this->x += pos.x;
        this->y += pos.y;
        this->z += pos.z;
        return *this;
    }

};

#endif // POSITION3_H
