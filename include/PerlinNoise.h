#ifndef PERLINNOISE_H
#define PERLINNOISE_H

#include <vector>

class PerlinNoise
{

    public:

    static const int DIMENSION_X = World::WORLD_SIZE_X;
    static const int DIMENSION_Y = World::WORLD_SIZE_Z;


    double terrain[DIMENSION_X][DIMENSION_Y];

    void createPerlin(){

    }

    void normalize(){

    }


};

#endif // PERLINNOISE_H
