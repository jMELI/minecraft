#ifndef WOODBLOCK_H
#define WOODBLOCK_H

#include "Block.h"

class WoodBlock : public Block
{
    public:
        WoodBlock() : Block(CubeGenerator::createWood(), BlockType::WoodBlock){

        }

        bool collides() const override {
            return true;
        }

};

#endif // WOODBLOCK_H
