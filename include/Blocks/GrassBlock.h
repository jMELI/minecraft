#ifndef GRASSBLOCK_H
#define GRASSBLOCK_H

#include "Block.h"

class GrassBlock : public Block
{
    public:
        GrassBlock() : Block(CubeGenerator::createGrass(), BlockType::GrassBlock){

        }

        bool collides() const override{
            return true;
        }

};

#endif // GRASSBLOCK_H
