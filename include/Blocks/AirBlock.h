#ifndef AIRBLOCK_H
#define AIRBLOCK_H

#include "Block.h"

class AirBlock : public Block
{

    float lightValue = 0;

    public:
        AirBlock() : Block(BlockType::AirBlock){

        }

        void render(float x, float y, float z) const override{
            //Dont render Air
        }

        bool collides() const override {
            return false;
        }

        float getLightValue() const {
            return lightValue;
        }

        void setLightValue(float v){
            lightValue = v;
        }

};

#endif // AIRBLOCK_H
