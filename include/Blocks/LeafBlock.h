#ifndef LEAFBLOCK_H
#define LEAFBLOCK_H

#include "Block.h"

class LeafBlock : public Block
{
    public:
        LeafBlock() : Block(CubeGenerator::createLeaves(), BlockType::LeafBlock) {
        }

    bool collides() const override{
        return true;
    }
};

#endif // LEAFBLOCK_H
