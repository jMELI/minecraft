#ifndef BLOCK_H
#define BLOCK_H

#include "Cube.h"
#include "World.h"

enum class UpdateResult{
    NONE,
    REMOVE,
    FALL
};

class Block
{
    private:
        float lightTop = 1;
        float lightBottom = 1;
        float lightE = 0.5;
        float lightW = 0.5;
        float lightS = 0.5;
        float lightN = 0.5;

    protected:
        Cube cube;
        bool visible;
        BlockType type;

    public:

        Block(BlockType t){
            visible = true;
            type = t;
        }

        Block(Cube cube, BlockType t) : Block(t) {
            this->cube = cube;
        }

        virtual UpdateResult update(float delta) {
            return UpdateResult::NONE;
        }

        virtual void render(float x, float y, float z) const {
            if(visible){
                cube.voxelRender(x,y,z,lightTop,lightBottom, lightE, lightW, lightN, lightS);
            }
        }

        virtual bool collides() const {
            return false;
        }

        virtual void setVisible(bool vis) final {
            visible = vis;
        }

        virtual bool getVisible() const final {
            return visible;
        }

        virtual BlockType getType() const final {
            return type;
        }

        virtual void setLightValues(float top, float bot, float e, float w, float n, float s){
            lightTop = top;
            lightBottom = bot;
            lightE = e;
            lightN = n;
            lightS = s;
            lightW = w;
        }

        virtual ~Block(){

        }
};

#endif // BLOCK_H
