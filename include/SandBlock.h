#ifndef SANDBLOCK_H
#define SANDBLOCK_H


class SandBlock : public Block
{
    public:
        SandBlock() : Block(CubeGenerator::createSand(), BlockType::SandBlock) {}

        bool collides() const override{
            return true;
        }
};

#endif // SANDBLOCK_H
