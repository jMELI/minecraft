#ifndef PLAYER_H
#define PLAYER_H

class World;

#include "Position3.h"
#include "Utils.h"
#include <iostream>

class Player
{
    World& world;

    Position3 position;
    float rotation = 20;
    float headAngle = 90;

    float speed = 3;

    // 0  = no move
    // 1  = forward
    // -1 = backward
    int moving = 0;

    float velocityY = 0;
    float gravity = -10;

    public:

        Player(World& w) : world(w){

        }


        void setPosition(float x, float y, float z){
            position.x = x;
            position.y = y;
            position.z = z;
        }

        void setMoving(int mov){
            moving = mov;
        }

        void setRotation(float rot){
            rotation = rot;
        }

        void rotateBy(float angle){
            rotation += angle;
        }

        void setHeadAngle(float angle){
            headAngle = angle;
            if(angle > 160) headAngle = 160;
            if(angle < 1) headAngle = 1;
        }

        void rotateHeadBy(float angle){
            setHeadAngle(headAngle + angle);
        }

        void jump(){
            velocityY = 5;
        }

        int getMoving(){
            return moving;
        }

        Position3& getPosition(){
            return position;
        }

        Position3 getEyesPosition(){
            Position3 p = position;
            p.y += 1;
            return p;
        }

        float getRotation(){
            return rotation;
        }

        float getHeadAngle(){
            return headAngle;
        }

        void update(float delta);


    private:
        void move(float delta);
};




#endif // PLAYER_H
