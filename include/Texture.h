#ifndef TEXTURE_H
#define TEXTURE_H

#include <GL/freeglut.h>
#include "SOIL.h"
#include <string>
#include <iostream>

class Texture
{

    GLuint texture;
    std::string name;

    public:
        Texture(std::string name) {
            this->name = name;
        }

        void load() {

            texture = SOIL_load_OGL_texture
            (
                name.c_str(),
                SOIL_LOAD_AUTO,
                SOIL_CREATE_NEW_ID,
                SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y | SOIL_FLAG_NTSC_SAFE_RGB | SOIL_FLAG_COMPRESS_TO_DXT
            );
            if( 0 == texture )
            {
                std::cout << "SOIL loading error:" <<  SOIL_last_result() << std::endl;
            }
            else{
                std::cout << "Texture loaded :" <<  name << std::endl;
            }

        }

        GLuint getTexture(){
            return texture;
        }

};

#endif // TEXTURE_H
