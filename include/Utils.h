#ifndef UTILS_H
#define UTILS_H

#define PI 3.141592
#include <math.h>

class Utils
{
    public:

    static float radToDegrees(float rads){
        return rads * 180.0 / PI;
    }

    static float degreesToRad(float deg){
        return deg * PI / 180.0;
    }

};

#endif // UTILS_H
