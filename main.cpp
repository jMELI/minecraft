#include <iostream>
#include <GL/freeglut.h>
#include "include/Assets.h"
//#include "include/Block.h"
#include "include/Camera.h"
#include "include/CubeGenerator.h"
#include "include/Player.h"
#include "include/World.h"
#include <time.h>
#include <iostream>


using namespace std;

World world;

long lastMiliseconds = 0;

int width   = 800;
int height  = 600;

void resizeWindow(int w, int h){
    if (h == 0)
		h = 1;
	float ratio =  w * 1.0 / h;
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glViewport(0, 0, w, h);
	gluPerspective(50.0f, ratio, 0.1f, 100);
	glMatrixMode(GL_MODELVIEW);
	width = w;
	height = h;
}

//float angle = 0;

void render(){
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();

    glEnable(GL_TEXTURE_2D);
    float delta = static_cast<float>((clock() - lastMiliseconds)/1000.0);
    lastMiliseconds = clock();
    world.getCamera().lookAt();
    world.update(delta);
    //glRotatef(angle, 1, 0, 1);
    //GLfloat light_position[] = { 0, 1, 0, 0 };
    //glLightfv(GL_LIGHT0, GL_POSITION, light_position);
    //glRotatef(-angle, 1, 0, 1);
    world.render();
    glutSwapBuffers();
    //angle += delta*5;
}


void mousePressed(int button, int state, int x, int y){
    if(state == GLUT_UP){
        if(button == GLUT_LEFT_BUTTON){
            world.mousePressed(-1);
        }
        else{
            world.mousePressed(1);
        }
    }
}

void mouseMove(int x, int y){
    static bool init = false;
    static int lastX;
    static int lastY;
    if(!init){
        lastX = x;
        lastY = y;
        init = true;
    }
    world.mouseMoved(lastX, lastY, x, y);
    lastX = x;
    lastY = y;

    if( x > width - 100 || x < 100 || y < 100 || y > height - 100 ){
        glutWarpPointer(width/2, height/2);
        lastX = width/2;
        lastY = height/2;
    }
}

void keyPressed(unsigned char key, int x, int y){
    world.keyPressed(key);
}

void keyReleased(unsigned char key, int x, int y){
    world.keyReleased(key);
}


int main(int argc, char** argv)
{
    glutInit(&argc, argv);
    glutSetOption(GLUT_MULTISAMPLE, 4);
    glutInitDisplayMode(GLUT_DEPTH| GLUT_DOUBLE| GLUT_RGBA| GLUT_MULTISAMPLE);
    glutInitWindowSize(width, height);
    glutCreateWindow("Minecraft");

    glEnable(GL_DEPTH_TEST);
    //glEnable(GL_LIGHTING);
    //glEnable(GL_LIGHT0);

    //GLfloat a[4] = {0.6,0.6,0.6,1};
    //glLightfv(GL_LIGHT0, GL_AMBIENT, a);
    //GLfloat d[4] = {0.8,0.8,0.8,1};
    //glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT,a);
    //glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE,d);

    glutSetCursor(GLUT_CURSOR_NONE);

    Assets::load();
    world.create();

    glutIdleFunc(render);
    glutDisplayFunc(render);
    glutReshapeFunc(resizeWindow);
    glutMouseFunc(mousePressed);
    glutMotionFunc(mouseMove);
    glutPassiveMotionFunc(mouseMove);
    glutKeyboardFunc(keyPressed);
    glutKeyboardUpFunc(keyReleased);
    glutFullScreen();
    glutMainLoop();

    return 0;
}
