#include "include/Assets.h"

Texture Assets::grassTop = Texture("textures/grassTop.png");
Texture Assets::grassSide = Texture("textures/grassSide.png");
Texture Assets::stone = Texture("textures/stone.png");
Texture Assets::woodSide = Texture("textures/woodSide.png");
Texture Assets::woodUp = Texture("textures/woodUp.png");
Texture Assets::leaves = Texture("textures/leaves.png");
Texture Assets::cobblestone = Texture("textures/cobblestone.png");
Texture Assets::diamond = Texture("textures/diamond.png");
Texture Assets::iron = Texture("textures/iron.png");
Texture Assets::sand = Texture("textures/sand.png");

